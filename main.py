import base64
from typing import Annotated, Union
import cv2
from fastapi import FastAPI, File, Form, UploadFile

from fastapi.middleware.cors import CORSMiddleware
import numpy as np

import routes.celcius as celcius
import routes.users as users
import routes.lights as lights


from sqlalchemy.orm import Session

import crud
import models
import schemas
from database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)


app = FastAPI()
app.include_router(celcius.router)
app.include_router(users.router)
app.include_router(lights.router)
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"Hello": "World"}

# http://127.0.0.1:8000/items/1?q=worawit&r=1


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str | None = None, r: int | None = None):
    return {"item_id": item_id, "q": q, "r": r}


@app.post("/files/")
async def create_file(file: Annotated[bytes, File()]):
    return {"file_size": len(file)}


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    return {"filename": file.filename}


def draw_text(image, text, color,  org):
    thickness = 2
    fontScale = 1
    image = cv2.putText(image, text, org, cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale, color, thickness, cv2.LINE_AA)
    return image


@app.post("/label/")
async def label(text: Annotated[str, Form()],
                r: Annotated[int, Form()] = 0,
                g: Annotated[int, Form()] = 0,
                b: Annotated[int, Form()] = 0,
                x: Annotated[int, Form()] = 100,
                y: Annotated[int, Form()] = 100,
                file: UploadFile = None):
    contents = await file.read()
    nparr = np.frombuffer(contents, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    result = draw_text(image, text, (b, g, r), (x, y))
    _, img_encoded = cv2.imencode('.jpg', result)
    result_data_uri = f"data:image/jpeg;base64,{base64.b64encode(img_encoded).decode()}"
    return {"result": result_data_uri}
