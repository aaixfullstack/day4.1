import cv2

image = cv2.imread('cat.jpg')
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
edge = cv2.Canny(gray_image, 100, 100)
blur_image = cv2.blur(gray_image, (10, 10))
cv2.imshow('cat', image)
cv2.imshow('gray cat', gray_image)
cv2.imshow('edge', edge)
cv2.imshow('blur_image', blur_image)
cv2.waitKey(0)
