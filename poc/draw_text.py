import cv2
path = 'cat.jpg'
x = 150
y = 100
r = 0   # 0-255
g = 0
b = 255  # FF


text = 'HELLO BUU'

color = (b, g, r)
org = (x, y)


def draw_text(path, text, color,  org):
    thickness = 2
    fontScale = 1
    image = cv2.imread(path)
    image = cv2.putText(image, text, org, cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale, color, thickness, cv2.LINE_AA)
    return image


image = draw_text(path, text, color, org)
cv2.imshow('result', image)
cv2.waitKey(0)
