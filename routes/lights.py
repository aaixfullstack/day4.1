from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(prefix="/lights")

class Light(BaseModel):
    id: str
    status: bool

lights = {
  "l1": False,
  "l2": False,
  "l3": False,
  "l4": False,
  "l5": False,
}

@router.get("/")
def get_lights():
    return lights

@router.get("/{light_id}")
def get_light(light_id: str):
    return { "id": light_id, "status": lights[light_id] }

@router.put("/")
def set_light(light: Light):
    lights[light.id] = light.status
    return { "id": light.id, "status": light.status }
  
@router.put("/{light_id}/{status}")
def set_light(light_id: str, status: bool):
    lights[light_id] = status
    return { "id": light_id, "status": lights[light_id] }