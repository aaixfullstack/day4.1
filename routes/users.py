from database import SessionLocal
from sqlalchemy.orm import Session
import schemas
from fastapi import Depends, APIRouter, HTTPException

import crud
import models
import schemas

# Dependency


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


router = APIRouter(prefix="/users")


@router.post("/")  # สร้างใหม่
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@router.get("/")  # ดูทั้งหมด
def get_users(db: Session = Depends(get_db)):
    users = crud.get_users(db)
    return users


@router.delete("/{user_id}")  # ลบ
def delete_user(user_id: int, db: Session = Depends(get_db)):
    user = crud.delete_user(db=db, user_id=user_id)
    return user


@router.put("/{user_id}")  # แก้ไข
def update_user(user_id: int, user: schemas.User, db: Session = Depends(get_db)):
    edited_user = crud.update_user(db=db, user=user, user_id=user_id)
    return edited_user


# @router.get("/{user_id}")  # ดูแค่คนเดียว
# def get_user(user_id: int):
#     for user in users:
#         if user.id == user_id:
#             return user
#     return None
