python -m venv .env
activate .env
pip install -r requirements.txt
uvicorn main:app --reload --port 3000
